<%--
  Created by IntelliJ IDEA.
  User: Kélio
  Date: 25/09/2020
  Time: 11:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Exercice 1</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>
    <select id="selectMethod">
        <option selected value="GET">Get</option>
        <option value="POST">Post</option>
        <option value="PUT">Put</option>
    </select>

    <button id="sendRequest">Envoyer</button>

    <p id="Content"></p>

    <script>
        $("#sendRequest").click( function () {
            $.ajax({
               url: "http://localhost:8080/TP0-1.0/1",
               type: $("#selectMethod").val(),
               success: ShowContent
            });
        });

        function ShowContent(data) {
            $("#Content").text(data);
        }
    </script>
</body>
</html>
