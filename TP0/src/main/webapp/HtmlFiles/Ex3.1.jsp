<%--
  Created by IntelliJ IDEA.
  User: Kélio
  Date: 25/09/2020
  Time: 12:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercice 3</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>

<form id="form1" onsubmit="return false;">
    <label for="UserNameInput">Login</label>
    <input id="UserNameInput" class="fieldToSend" name="UserName" placeholder="Entrez un nom"/>
    <label for="UserPaswword">Password</label>
    <input id="UserPaswword" class="fieldToSend" name="Password" placeholder="Entrez un mot de passe"/>
    <button onclick="sendData()">Envoyer</button>
</form>

<p id="Content"></p>

<script>
    function sendData() {
        let dataObj = {};

        $(".fieldToSend").each(function (index, element) {
            dataObj[$(element).prop("name")] = $(element).val();
        });

        $.ajax({
            data: dataObj,
            type: "POST",
            url: "http://localhost:8080/TP0-1.0/3-1",
            success: ShowContent
        });
    }

    function ShowContent(data) {
        $("#Content").text(data);
    }
</script>
</body>
</html>
