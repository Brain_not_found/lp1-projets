<%--
  Created by IntelliJ IDEA.
  User: Kélio
  Date: 25/09/2020
  Time: 12:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercice 2</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>
<button id="sendRequest">Liste</button>

<div id="Content"></div>

<script>
    $("#sendRequest").click( function () {
        $.ajax({
            url: "http://localhost:8080/TP0-1.0/2",
            type: "GET",
            success: ShowContent
        });
    });

    function ShowContent(data) {
        $("#Content").append(data);
    }
</script>
</body>
</html>
