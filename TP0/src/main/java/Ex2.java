import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Ex2 extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        ArrayList<String> array = new ArrayList<String>() {
            {
                add("voiture");
                add("fourchette");
                add("ordinateur");
                add("stylo");
                add("grue");
                add("casque");
                add("chaise");
                add("volet");
                add("vélo");
            }
        };

        out.println("<select>");
        for (String object: array) {
            out.println("<option>" + object + "</option>");
        }
        out.println("</select>");
    }
}
